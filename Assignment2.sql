/*CREATE DATABASE SQLAssignmentMovie2*/
use SQLAssignmentMovie2

Drop TABLE IF EXISTS Actors
CREATE TABLE Actors(
actorId INT PRIMARY KEY IDENTITY(1,1),
name VARCHAR(50),
gender VARCHAR(10),
dob DATE 
)
DROP TABLE IF EXISTS ActorMovieTable
Drop TABLE IF EXISTS Movies
Drop TABLE IF EXISTS Producers
CREATE TABLE Producers(
producerId INT PRIMARY KEY IDENTITY(1,1),
name VARCHAR(50),
companyName VARCHAR(30),
companyEstDate DATE 
)

Drop TABLE IF EXISTS Movies
CREATE TABLE Movies(
movieId INT PRIMARY KEY IDENTITY(1,1),
name VARCHAR(50),
language VARCHAR(30),
producerId INT FOREIGN KEY REFERENCES Producers(producerId),
profit INT
)

DROP TABLE IF EXISTS ActorMovieTable 
CREATE TABLE ActorMovieTable(
movieId INT FOREIGN KEY REFERENCES Movies(movieId),
actorId INT FOREIGN KEY REFERENCES Actors(actorId),
)



/*
INSERT INTO Actors VALUES('Mila Kunis','Female','1986/11/14')
INSERT INTO Actors VALUES('Robert DeNiro','Male','1957/07/10')
INSERT INTO Actors VALUES('George Michael','Male','1978/11/23')
INSERT INTO Actors VALUES('Mike Scott','Male','1969/08/06')
INSERT INTO Actors VALUES('Pam Halpert','Female','1996/09/26')
INSERT INTO Actors VALUES('Dame Judi Dench','Female','1947/04/05')


Producers
INSERT INTO Producers VALUES('Sunny','Fox','1998/05/14')
INSERT INTO Producers VALUES('Arun','Bull','2004/09/11')
INSERT INTO Producers VALUES('Tom','Hanks','1987/11/03')
INSERT INTO Producers VALUES('Zeshan','Male','1996/11/14')
INSERT INTO Producers VALUES('Nicole','Team Coco','1992/09/26')



Movies
INSERT INTO Movies VALUES('Rocky','English',1,10000)
INSERT INTO Movies VALUES('Rocky','Hindi',3,3000)
INSERT INTO Movies VALUES('Terminal','English',4,300000)
INSERT INTO Movies VALUES('Rambo','Hindi',2,93000)
INSERT INTO Movies VALUES('Rudy','English',5,9600)


INSERT INTO ActorMovieTable VALUES (1,1),(1,3),(1,5)
INSERT INTO ActorMovieTable VALUES (2,6),(2,5),(2,4),(2,2)
INSERT INTO ActorMovieTable VALUES (3,3),(3,2)
INSERT INTO ActorMovieTable VALUES (4,1),(4,6),(4,3)
INSERT INTO ActorMovieTable VALUES (5,2),(5,5),(5,3)
*/


/*  Update Profit of all the movies by +1000 where producer name contains 'run'*/
UPDATE Movies
SET profit = profit + 1000
WHERE producerId in (
SELECT p.producerId
FROM Movies
JOIN Producers p ON
p.producerId = Movies. producerId
WHERE p.name LIKE '%run%'
)


/*Find duplicate movies having the same name and their count*/
SELECT 
	name AS [Movie Name],
	COUNT(*) AS [Total Count]
FROM Movies 
GROUP BY name
HAVING COUNT(*) >1


/* Find the oldest actor/actress for each movie*/

SELECT 
	T.[Actor Name],
	T.name,
	T.dob
FROM 
(SELECT
	m.movieId,
	m.name,
	RANK() OVER(PARTITION BY m.movieId ORDER BY a.dob) RANK,
	a.actorId,
	a.name AS [Actor Name],
	a.dob
FROM Movies m
JOIN ActorMovieTable am ON
am.movieId = m.movieId
JOIN Actors a ON
a.actorId = am.actorId)
T
WHERE T.RANK = 1



/*List of producers who have not worked with actor Pam Halpert*/
SELECT DISTINCT
	p.producerId,
	p.name AS [Producer Name],
	p.companyName AS [Company Name],
	p.companyEstDate AS [Company Est. Date]
FROM Producers P
WHERE P.producerId NOT IN (
SELECT p.producerId
FROM Movies m
JOIN Producers p ON
m.producerId = p.producerId
JOIN ActorMovieTable am ON
am.movieId = m.movieId 
JOIN Actors a ON
a.actorId = am.actorId
WHERE a.name = 'Pam Halpert')

/* List of pair of actors who have worked together in more than 2 movies*/

SELECT 
	T2.[Actor1 Name],
	T2.[Actor2 Name],
	Count(T2.movieId) AS [No. of Movies]
FROM
(SELECT 
	T.[Actor1 Name],
	T.[Actor2 Name],
	am.movieId,
	count(am.movieId) AS [Pair actors]
FROM
(
SELECT 
	a1.actorId AS a1Id,
	a1.name AS [Actor1 Name],
	a2.actorId,
	a2.name AS [Actor2 Name]
FROM Actors a1
JOIN Actors a2 ON
a1.actorId <> a2.actorId

WHERE a1.actorId < a2.actorId
)T JOIN ActorMovieTable am ON
am.actorId = T.a1Id OR am.actorId = T.actorId
GROUP BY T.[Actor1 Name],T.[Actor2 Name],am.movieId
HAVING count(am.movieId)>=2
) T2
GROUP BY T2.[Actor1 Name],T2.[Actor2 Name]
HAVING COUNT(T2.movieId)>=2


/* Add non-clustered index on profit column of movies table */
DROP INDEX IF EXISTS tblmovies_profit ON Movies.profit
CREATE NONCLUSTERED INDEX tblmovies_profit
ON Movies (profit ASC)



/* Create stored procedure to return list of actors for given movie id */

DROP PROCEDURE IF EXISTS ListOfActors
GO
CREATE PROCEDURE ListOfActors @mId INT
AS
SELECT 
	a.name,
	a.gender,
	a.dob
FROM Movies m
JOIN ActorMovieTable am ON 
am.movieId = m.movieId 
JOIN Actors a ON
a.actorId = am.actorId
WHERE m.movieId = @mId
GO

exec ListOfActors @mId = 3

/* Create a function to return age for given date of birth */
GO
CREATE FUNCTION getAge (@dob DATE)
RETURNS INT AS
BEGIN
DECLARE @age INT
SET @age= DATEDIFF(hour,@dob,GETDATE())/8766 
RETURN @age
END
GO

SELECT 
	name AS [Actor Name],
	dbo.getAge(dob) AS [Age],
	gender AS [Gender]
FROM Actors

/* Create a stored procedure to increase the profit (+100) of movies with given Ids (comma separated) */
GO
CREATE PROCEDURE increaseProfit @mId INT
AS
UPDATE Movies 
SET profit = profit +100
WHERE movieId = @mId
GO

EXEC increaseProfit @mId=2
EXEC increaseProfit @mId=5

SELECT name FROM sys.indexes 
select *
from Actors

select *
from Producers

select *
from Movies

select *
from ActorMovieTable

SELECT *
FROM Producers p 
LEFT OUTER JOIN Movies m ON
p.

SELECT *
from Producers

Producers
producerId,name,CompanyId

Company
comapnyId,
