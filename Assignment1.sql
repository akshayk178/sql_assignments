	/*create database SQLAssignment1*/
use SQLAssignment1

/*
create table classes(
classId int Primary key Identity(1,1),
Number int,
name varchar(50),
section varchar(10)
)


DROP TABLE IF EXISTS Teachers
create table Teachers(
teacherId int Primary key Identity(1,1),
name varchar(50),
dob date,
gender varchar(20)
)

DROP TABLE IF EXISTS Students
create table Students(
studentId int Primary Key Identity(1,1),
name varchar(50),
dob date ,
gender varchar(20),
classId int,
Foreign key(classId) references classes(classId)
)

DROP TABLE IF EXISTS TeacherClassMapping
create table TeacherClassMapping(
teacherId int foreign key references Teachers(teacherId)
classId int foreign key references classes(classId),

)


INSERT INTO classes (name,section,Number) values ('IX','A',201)
INSERT INTO classes (name,section,Number) values ('IX','B',202)
INSERT INTO classes (name,section,Number) values ('X','A',203)

INSERT INTO Teachers (name,dob,gender) values ('Lisa Kudrow','1985/06/08','Female')
INSERT INTO Teachers (name,dob,gender) values ('Monica Bing','1982/03/06','Female')
INSERT INTO Teachers (name,dob,gender) values ('Chandler Bing','1978/12/17','Male')
INSERT INTO Teachers (name,dob,gender) values ('Ross Geller','1993/01/26','Male')

INSERT INTO Students values ('Scotty Loman','2006/01/31','Male',1)
INSERT INTO Students values ('Adam Scott','2005/06/01','Male',1)
INSERT INTO Students values ('Natosha Beckles','2005/01/23','Female',2)
INSERT INTO Students values ('Lilly Page','2006/11/26','Female',2)
INSERT INTO Students values ('John Freeman','2006/06/14','Male',2)
INSERT INTO Students values ('Morgan Scott','2005/05/18','Male',3)
INSERT INTO Students values ('Codi Gass','2005/12/24','Female',3)
INSERT INTO Students values ('Nick Roll','2005/12/24','Male',3)
INSERT INTO Students values ('Dave Grohl','2005/02/12','Male',3)


INSERT INTO TeacherClassMapping values (1,1)
INSERT INTO TeacherClassMapping values (1,2)
INSERT INTO TeacherClassMapping values (2,2)
INSERT INTO TeacherClassMapping values (2,3)
INSERT INTO TeacherClassMapping values (3,3)
INSERT INTO TeacherClassMapping values (3,1)
*/

/*1. List of male students*/
SELECT
	s.studentId,
	s.name AS[Student Name],
	s.dob AS [Date of Birth],
	s.gender AS [Gender]
FROM Students s
WHERE gender = 'Male'

/*2.Find list of student older than 2005/01/01 */
SELECT *
FROM students
WHERE dob< '01/01/2005'

/*3.Youngest students in the class*/
SELECT top 1 *
FROM students 
ORDER BY dob

/*4. Find students with distinct birthdays*/
/*select *
from Students
where dob in (select distinct dob from Students)*/

SELECT *
FROM Students
WHERE dob in (
SELECT 
	dob
FROM Students 
GROUP by dob
HAVING count(dob)=1)

/*5. No of students in each class*/
SELECT 
	c.name AS [Class Name],
	count(*) AS [No. of students]
FROM Students s
JOIN classes c on c.classId = s.classId
GROUP BY c.name


/* 6.No of students in each section */
SELECT  
	c.section AS [Section], 
	count(*) AS [No. of students]
FROM Students s
JOIN classes c on 
s.classId = c.classId
GROUP BY c.section

/*7. No of classes taught by teacher */
SELECT 
	t.teacherId,
	count(*) AS [No. of Classes]
FROM Teachers t
JOIN TeacherClassMapping tc ON t.teacherId = tc.teacherId
GROUP BY t.teacherId

/*8.  List of teachers teaching Class X*/
SELECT 
	t.teacherId,
	t.name AS [Teacher Name],
	t.dob AS [Date of Birth],
	t.gender AS [Gender],
	c.name AS [Class Name]
FROM Teachers t
JOIN TeacherClassMapping tc ON
tc.teacherId =t.teacherId
JOIN classes c on 
tc.classId = c.classId
WHERE c.name = 'X'

/*10 Classes which have more than 2 teachers teaching */
SELECT
	c.name AS [Class Name], 
	count(*) AS [No. of Teachers]
FROM classes c
JOIN TeacherClassMapping tc on
c.classId = tc.classId
GROUP BY c.name
HAVING count(*)>2

/*11 List of students being taught by 'Lisa'*/
SELECT 
	s.studentId,
	s.name AS [Student Name],
	s.dob AS [Data of Birth],
	s.gender AS [Gender],
	t.name AS [Teacher Name]
FROM Teachers t
JOIN TeacherClassMapping tc ON
tc.teacherId = t.teacherId
JOIN Students s on
tc.classId = s.classId
WHERE t.name like 'Lisa%'


