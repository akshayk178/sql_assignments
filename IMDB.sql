CREATE DATABASE IMDB
USE IMDB

DROP TABLE IF EXISTS Actors
CREATE TABLE Actors(
actorId INT PRIMARY KEY IDENTITY(1,1),
Fname VARCHAR(25) NOT NULL,
Lname VARCHAR(25) NOT NULL,
gender VARCHAR(20) NOT NULL,
dob DATE NOT NULL,
bio TEXT
)

DROP TABLE IF EXISTS Producers
CREATE TABLE Producers(
producerId INT PRIMARY KEY IDENTITY(1,1),
Fname VARCHAR(25) NOT NULL,
Lname VARCHAR(25) NOT NULL,
gender VARCHAR(20) NOT NULL,
dob DATE NOT NULL,
bio TEXT
)

DROP TABLE IF EXISTS Movies
CREATE TABLE Movies(
movieId INT PRIMARY KEY IDENTITY(1,1),
yor DATE NOT NULL,
plot TEXT,
poster NVARCHAR(100),
producerId INT FOREIGN KEY REFERENCES Producers(producerId)
)

DROP TABLE IF EXISTS ActorMovieTable 
CREATE TABLE ActorMovieTable(
movieId INT FOREIGN KEY REFERENCES Movies(movieId),
actorId INT FOREIGN KEY REFERENCES Actors(actorId),
)

DROP TABLE IF EXISTS Genres
CREATE TABLE Genres(
genreId INT PRIMARY KEY IDENTITY(1,1),
name VARCHAR(25) NOT NULL
)

DROP TABLE IF EXISTS MovieGenreTable
CREATE TABLE MovieGenreTable(
movieId INT FOREIGN KEY REFERENCES Movies(movieId),
genreId INT FOREIGN KEY REFERENCES Genres(genreId)
)

SELECT *
FROM Actors

SELECT *
FROM Producers

SELECT *
FROM Movies

SELECT *
FROM ActorMovieTable

SELECT *
FROM Genres

SELECT *
FROM MovieGenreTable